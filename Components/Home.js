import React from 'react';
import { View, Text, StyleSheet, TextInput, Button, Keyboard} from 'react-native'
import { getMeteoByCity } from './../API/API.js'

/**
 * Component principal. Permet de rechercher une ville et d'afficher les informations météo.
 */
class Home extends React.Component {
    constructor(props) {
        super(props)
        this.searchedCity = ''
        this.state = { meteo: null }
    }

    /**
     * Fonction utilisée pour mettre la valeur saisie dans la variable searchedCity.
     * @param {*} pCity La ville saisie dans le champs texte.
     */
    setMeteoCity(pCity) {
        this.searchedCity = pCity
    }

    /**
     * Fonction pour lancer une recherche sur la ville saisie. Appelle l'API.
     */
    searchMeteoCity() {
        getMeteoByCity(this.searchedCity).then(res => {
            this.setState({meteo: res.data})
            Keyboard.dismiss()
        }).catch(err => {
            console.log(err)
        })
    }

    /**
     * Fonction permettant de retourner le composant avec la météo.
     */
    displayMeteo() {
        if (this.state.meteo) { 
            return (
                <View style={styles.card}>
                    <View style={styles.ville}>
                        <Text style={styles.villeText}>{this.searchedCity}</Text>
                    </View>
                    <View style={styles.description}>
                        <Text style={styles.descriptionText}>{this.state.meteo.weather[0].description}</Text>
                    </View>
                    <View style={styles.temperatures}>
                        <View style={styles.temperature}>
                            <Text style={styles.temperatureText}>{this.state.meteo.main.temp} °</Text>
                            <Text style={styles.temperatureLabel}>Température</Text>
                        </View>
                        <View style={styles.temperature}>
                            <Text style={styles.temperatureText}>{this.state.meteo.main.feels_like} °</Text>
                            <Text style={styles.temperatureLabel}>Ressentie</Text>
                        </View>
                    </View>
                    <View style={styles.temperatures}>
                        <View style={styles.temperature}>
                            <Text style={styles.temperatureText}>{this.state.meteo.main.humidity} %</Text>
                            <Text style={styles.temperatureLabel}>Humidité</Text>
                        </View>
                    </View>
                    <View style={styles.meteoFuture}>
                        <Button onPress={() => this.goToMeteoFuture()} title="Voir la météo pour les prochains jours"></Button>
                    </View>
                </View>
            )
        }
    }

    /**
     * Fonction pour consulter la météo sur les prochains jours.
     */
    goToMeteoFuture() {
        this.props.navigation.navigate("MeteoFuture", {lat: this.state.meteo.coord.lat, lon: this.state.meteo.coord.lon})
    }

    render() {
        return (
            <View style={styles.content}>
                <View style={styles.search}>
                    <TextInput style={styles.textInput} placeholder="Saisissez ici le nom de la ville" onChangeText={(text => this.setMeteoCity(text))}></TextInput>
                </View>
                <View style={styles.button}>
                    <Button title="Obtenir la météo" onPress={() => this.searchMeteoCity()}></Button>
                </View>
                <View style={styles.resultat}>
                    {this.displayMeteo()}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1
    },
    search: {
        flex: 2,
        justifyContent: 'center',
    },
    button: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    resultat: {
        flex:8
    },
    textInput: {
        marginRight: 10,
        marginLeft: 10,
        marginTop: 10,
        marginBottom: 10,
        padding: 5,
        fontSize: 18,
        backgroundColor: 'white'
    },
    card: {
        backgroundColor: 'lightgrey',
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20,
        flex: 1
    },
    ville: {
        flex: 1,
        alignItems: 'center'
    },
    villeText: {
        alignItems: 'center',
        fontSize: 28
    },
    description: {
        flex: 1,
        alignItems: 'center'
    },
    descriptionText: {
        alignItems: 'center',
        fontSize: 28,
        fontWeight: 'bold'
    },
    temperatures: {
        flex: 2,
        flexDirection: 'row'
    },
    temperature: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    temperatureText: {
        fontSize: 28,
        fontWeight: 'bold'
    },
    meteoFuture: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center'
    }

})
export default Home



