import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native'
import Jour from './Jour'
import { getMeteoFuture } from './../API/API.js'

class MeteoFuture extends React.Component {
    constructor(props) {
        super(props)
        this.state = { meteoFuture: null }
       
    }

    componentDidMount(){
        getMeteoFuture(this.props.navigation.state.params.lat, this.props.navigation.state.params.lon).then(res => {
            this.setState({meteoFuture: res.data.daily})
            console.log('Meteo' + this.state.meteoFuture[0].temp.day)
        }).catch(err => {
            console.log(err)
        })
    }

    render() {
        return (
            <View style={styles.content}>
                <View style={styles.titre}>
                    <Text style={styles.textMeteo}>Meteo future</Text>
                </View>
                <View style={styles.liste}>
                    <FlatList data={this.state.meteoFuture}  keyExtractor={(item) => item.dt.toString()} renderItem={({item}) => <Jour temp={item.temp.day} date={item.dt}/>}/>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    content: {
        flex: 1,
        //alignItems: 'center',
    },
    liste: {
        flex: 7,
    },
    titre: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textMeteo: {
        fontSize: 22
    }

})
export default MeteoFuture
