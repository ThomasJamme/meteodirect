import React from 'react';
import { View, Text, StyleSheet } from 'react-native'
import moment from 'moment'
import 'moment/locale/fr' 

class Jour extends React.Component {
    constructor(props) {
        super(props)
    }

    formatDate() {
        console.log(this.props.date)
        let date = moment.unix(this.props.date);
        date.locale("fr")
        moment.locale('fr')
        return date.format('dddd')
    }

    render() {
        return (
            <View style={styles.content}>
                <View style={styles.jour}><Text style={styles.tempText}>{ this.formatDate() }</Text></View>
                <View style={styles.temp}>
                    <Text style={styles.tempText}>{ this.props.temp }°</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    content: {
        flexDirection: 'row',
        flex: 1,
        backgroundColor: 'lightgrey',
        margin: 10,
        padding: 10

    },
    jour: {
        flex: 1,
        alignItems: 'center'
    },
    temp: {
        fontSize: 18,
        flex: 1,
        alignItems: 'center'
    },
    tempText: {
        fontSize: 18
    }

})
export default Jour
