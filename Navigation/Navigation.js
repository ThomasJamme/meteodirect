import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Home from '../Components/Home'
import MeteoFuture from '../Components/MeteoFuture'

const MeteoNavigator = createStackNavigator({
    // Page d'accueil permettant d'afficher les résultats météo pour une ville donnée.
    Home: {
        screen: Home,
        navigationOptions: {
            title: 'Meteo direct'
        }
    },
    MeteoFuture: {
        screen: MeteoFuture,
        navigationOptions: {
            title: 'Meteo des prochains jours'
        }
    },
})

export default createAppContainer(MeteoNavigator)
